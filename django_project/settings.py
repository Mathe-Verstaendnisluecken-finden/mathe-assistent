"""
Django settings for django_project project.

Generated by 'django-admin startproject' using Django 3.0.2.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.0/ref/settings/
"""

import os
import sys
import deploymentutils as du

# Build paths inside the project like this: os.path.join(BASEDIR, ...)
BASEDIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# ensure BASEDIR is as expected
assert os.path.isfile(os.path.join(BASEDIR, "manage.py"))



# DEVMODE should be False by default.
# Exceptions: 'runserver' command or explicitly set by ENV-Variable

# export DJANGO_DEVMODE=True; py3 manage.py some_custom_command
env_devmode = os.getenv("DJANGO_DEVMODE")
if env_devmode is None:
    DEVMODE = "runserver" in sys.argv
else:
    DEVMODE = (env_devmode.lower() == "true")



config = du.get_nearest_config("config.ini", devmode=DEVMODE)


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/

SECRET_KEY = config("SECRET_KEY")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = config("DEBUG", cast=bool)

# prevent accidentally using DEBUG == "False" (which evaluates to `True`)
assert DEBUG in (True, False)
assert DEVMODE in (True, False)

# BASEURL = config("BASEURL")

ALLOWED_HOSTS = config("ALLOWED_HOSTS", cast=config.Csv())


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # for improved testing experience
    'django_nose',

    # to safely render HTML
    'django_bleach',

    # the example app
    'mainapp.apps.MainAppConfig',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    # 'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'django_project.urls'

STATICFILES_DIRS = [config("STATICFILES").replace("__BASEDIR__", BASEDIR)]
TEMPLATEDIR = config("TEMPLATEDIR").replace("__BASEDIR__", BASEDIR)

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'django_project.wsgi.application'


# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASEDIR, 'db.sqlite3'),
    }
}


# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]



LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '{asctime} {levelname} {module} {process:d} {thread:d} {message}',
            'style': '{',
        },
        'simple': {
            'format': '{asctime} {levelname} {message}',
            'style': '{',
        },
    },
    'handlers': {
        'file1': {
            'level': 'WARNING',
            'class': 'logging.FileHandler',
            'filename': config("DJANGO_LOGFILE").replace("__BASEDIR__", BASEDIR),
            'formatter': 'verbose',
        },
        'file2': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': config("MYAPP_LOGFILE").replace("__BASEDIR__", BASEDIR),
            'formatter': 'simple',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['file1'],
            'level': 'WARNING',
            'propagate': True,
        },
        'myapp': {
            'handlers': ['file2'],
            'level': 'DEBUG',
            'propagate': True,
        },
    },
}


TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'

# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = config("STATIC_ROOT").replace("__BASEDIR__", BASEDIR)

SITE_ID = 1

BLEACH_ALLOWED_TAGS = ['p', 'b', 'i', 'u', 'em', 'strong', 'a',
                       'h1', 'h2', 'h3', 'h4', 'h5', 'ul', 'ol', 'li', 'pre', 'code', 'script',
                       "table", "thead", "tr", "th", "td", "tbody", "hr", "img", "div"]


def allow_attributes(tag, name, value):
    """
    Use callable to decide which attributes we allow.
    Background: "script" should only be allowed for type="math/tex".

    see also: https://bleach.readthedocs.io/en/latest/clean.html#allowed-tags-tags
    """
    if name in ['href', 'title', 'style', 'class']:
        return True
    elif tag == "script" and name == "type" and value.startswith("math/tex"):
        return True
    elif tag == "img":
        return True
    else:
        return False


BLEACH_ALLOWED_ATTRIBUTES = allow_attributes
BLEACH_STRIP_TAGS = False
BLEACH_STRIP_COMMENTS = False


