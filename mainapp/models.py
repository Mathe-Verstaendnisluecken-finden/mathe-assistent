from django.db import models


# useful commands:
# python manage.py flush                (löscht die Datenbank)
# python manage.py makemigrations
# python manage.py migrate
# python manage.py populate_db_from_md_files        (erstellt die Datenbank für den Webserver)



class BaseModel(models.Model):
    """
    prevent PyCharm from complaining on .objects-attribute
    source: https://stackoverflow.com/a/56845199/333403
    """

    objects = models.Manager()

    class Meta:
        abstract = True

    def __repr__(self):
        name = getattr(self, "name", "<noname>")
        return f'<{type(self).__name__} "{name}">'

    def repr(self):
        return repr(self)


class OverviewDocument(BaseModel):
    id = models.AutoField(primary_key=True)
    level = models.PositiveIntegerField()
    upper_level = models.ForeignKey("self", null=True, on_delete=models.CASCADE)
    page_id = models.CharField(max_length=1000, null=True, blank=False)
    page_name = models.CharField(max_length=1000, null=True, blank=False)
    content = models.TextField(blank=True)
    file_path = models.CharField(max_length=1000, null=True, blank=False)

    def __repr__(self):
        name = getattr(self, "page_name", "<noname>")
        return f'<{type(self).__name__} "{name}">'


class ContentDocument(BaseModel):
    id = models.AutoField(primary_key=True)
    page_id = models.CharField(max_length=1000, null=True, blank=False)
    page_name = models.CharField(max_length=1000, null=True, blank=False)
    category = models.ForeignKey(OverviewDocument, null=True, on_delete=models.CASCADE)
    requirements = models.ManyToManyField("self", symmetrical=False)
    content = models.TextField(blank=True)
    file_path = models.CharField(max_length=1000, null=True, blank=False)
    exercise_path = models.CharField(max_length=1000, null=True, blank=False)


    def __repr__(self):
        name = getattr(self, "page_name", "<noname>")
        return f'<{type(self).__name__} "{name}">'


class MenuEntry(BaseModel):
    id = models.AutoField(primary_key=True)
    level = models.PositiveIntegerField()
    entry_name = models.CharField(max_length=1000, null=True, blank=False)
    entry_id = models.CharField(max_length=1000, null=True, blank=False)
    sub_categorys = models.ManyToManyField("self")


    def __repr__(self):
        name = getattr(self, "entry_name", "<noname>")
        return f'<{type(self).__name__} "{name}">'
