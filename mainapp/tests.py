from django.test import TestCase
from django.urls import reverse
import glob
import os
from django.conf import settings
from django.core.management import call_command
from . import views
from . import models

from ipydex import IPS

# The tests can be run with
# `python manage.py test`
# `python manage.py test --rednose --nocapture` # with colors


class TestMainApp1(TestCase):
    # alle Funktionen hier müssen mit "test_" beginnen
    def setup(self):
        pass

    def test_home_page1(self):
        # get url by its unique name, see urls.py
        url = reverse("landingpage")
        res = self.client.get(url)

        # `utc` means "unit test comment"
        # this is a simple mechanism to ensure the desired content actually was delivered
        self.assertEquals(res.status_code, 200)
        self.assertContains(res, "utc_landing_page")
        self.assertNotContains(res, "utc_debug_page")

    def test_debug1(self):
        res = self.client.get(reverse("debugpage0"))
        self.assertEquals(res.status_code, 200)

        url = reverse("debugpage_with_argument", kwargs={"xyz": 2})

        print("\n-> Debug-URL with argument:", url)
        # this will start the interactive shell inside the view
        # res = self.client.get(url)

        # this will deliberately provoke an server error (http status code 500)
        res = self.client.get(reverse("debugpage_with_argument", kwargs={"xyz": 2}))
        self.assertEquals(res.status_code, 500)

    def test_filelist(self):
        views.create_menu("/home/frieder/PycharmProjects/Webapplikation 2/django-project-template/mainapp/content")

    def test_content_page(self):
        url = reverse("md_inhalt", kwargs={"content_code": "zz_unittest"})
        res = self.client.get(url)
        self.assertEquals(res.status_code, 404)

        call_command("populate_db_from_md_files", silent=True)

        # get url by its unique name, see urls.py

        url = reverse("md_inhalt", kwargs={"content_code": "zz_unittest"})
        res = self.client.get(url)

        # `utc` means "unit test comment"
        # this is a simple mechanism to ensure the desired content actually was delivered
        self.assertEquals(res.status_code, 200)

        self.assertContains(res, "utc_md_unittest_content")
        self.assertContains(res, "<strong>unittest_strong</strong>")
        self.assertContains(res, '<script type="math/tex">')

    def test_overview_page(self):
        call_command("populate_db_from_md_files", silent=True)
        url = reverse("overview", kwargs={"page_code": "kat_ReOp_komplex_Zahlen"})
        res = self.client.get(url)
        self.assertEquals(res.status_code, 200)

        test_string = """
    <li>
        <a href="/md_inhalt/koZa_ReOp_Multiplikation">
            Multiplikation komplexer Zahlen
        </a>
    </li>
"""
        self.assertContains(res, test_string)


    def test_menu(self):
        call_command("populate_db_from_md_files", silent=True)
        path = os.path.join(settings.BASEDIR, "mainapp", "content")
        list_of_files = glob.glob(path + "/kat_*.md")

        kat_counter = len(list_of_files)

        url = reverse("md_inhalt", kwargs={"content_code": "zz_unittest"})
        res = self.client.get(url)
        self.assertContains(res, 'Anzahl der erwarteten Menüpunkte: ' + str(kat_counter))

    def test_populate_db1(self):
        call_command("populate_db_from_md_files", silent=True)

        overview_docs = models.OverviewDocument.objects.all()
        self.assertEqual(len(overview_docs), 4)

        content_docs = models.ContentDocument.objects.all()
        self.assertEqual(len(content_docs), 8)

        koza_div = models.ContentDocument.objects.filter(page_id="koZa_ReOp_Division").first()

        self.assertEqual(len(koza_div.requirements.all()), 2)

        self.assertEqual(koza_div.category.page_id, "kat_ReOp_komplex_Zahlen")

        # bug: unexpected requirement
        koza_mul = models.ContentDocument.objects.filter(page_id="koZa_ReOp_Multiplikation").first()
        self.assertEqual(len(koza_mul.requirements.all()), 1)

