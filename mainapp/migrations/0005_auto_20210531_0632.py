# Generated by Django 3.1.4 on 2021-05-31 06:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0004_contentdocument_exercise_path'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contentdocument',
            name='requirements',
            field=models.ManyToManyField(to='mainapp.ContentDocument'),
        ),
    ]
