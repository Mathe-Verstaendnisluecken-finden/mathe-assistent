---
content_type: article
page_id: koZa_ReOp_Division
page_name: Division komplexer Zahlen
main_category: kat_komplex_Zahlen
sub_category: kat_ReOp_komplex_Zahlen
requirements:  koZa_ReOp_Addition_Subtraktion
               koZa_ReOp_Multiplikation
exercise_path: Division
---

# Division komplexer Zahlen

---


<!--Quelle: <https://www.w3schools.com/howto/howto_css_equal_height.asp> -->


<div class="col-container" markdown="1">
  <div class="col" markdown="1">

**Allgemein**

---
  
$z_1 \over z_2$

$\Leftrightarrow {a_1 + b_1 i \over a_2+b_2i}$

$\Leftrightarrow {\left( a_1 + b_1 i \right) \cdot \left( a_2-b_2 i \right) \over \left( a_2+b_2i \right) \cdot \left( a_2-b_2 i \right)}$

$\Leftrightarrow {\left( a_1 + b_1 i \right) \cdot \left( a_2-b_2 i \right) \over a_2^2 -b_2^2i^2}$ ([Multiplikation komplexer Zahlen](koZa_ReOp_Multiplikation))

$\Leftrightarrow {\left( a_1 \cdot a_2+b_1 \cdot b_2 \right)+\left( -a_1 b_2+a_2 b_1 \right)i \over a_2^2 +b_2^2}$

$\Leftrightarrow {{a_1 \cdot a_2+b_1 \cdot b_2 \over a_2^2 +b_2^2}+{a_2 b_1-a_1 b_2 \over a_2^2 +b_2^2}i}$

    
  </div>

  <div class="col col-100" markdown="1">

**Beispiel**

---

$~$

${-16+79i}\over{3+8i}$

$\Leftrightarrow {{\left( -16+79i \right) \cdot \left( 3-8i \right)}\over{\left( 3+8i \right) \cdot \left( 3-8i \right)}}$

$\Leftrightarrow {{\left( -16 \cdot 3+79 \cdot 8 \right)+\left( +16 \cdot 8+3 \cdot 79 \right)}\over{3^2-8}^2}$
[Multiplikation komplexer Zahlen](Multiplikation)

$\Leftrightarrow {{-48+632}\over{9+64}} + {{\left( +128+237 \right)i}\over{9+64}}$

$\Leftrightarrow 8+5i$


 
    
  </div>
</div> 


