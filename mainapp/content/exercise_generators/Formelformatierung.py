#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 23 12:20:53 2021

@author: frieder
"""


def pz(komplexeZahl):
    return str(int(komplexeZahl.real))+"+"+str(int(komplexeZahl.imag))+"i"


def change(tochange):
    chars = []
    for char in tochange:
        chars.append(char)

    formel = False

    for i in range(len(chars)):
        if chars[i] == "$":
            if formel:
                formel = False
            else:
                formel = True

        if formel:
            if chars[i] == "(":
                chars[i] = "\\left( "
            elif chars[i] == ")":
                chars[i] = " \\right)"
            elif chars[i] == "*":
                chars[i] = " \\cdot "
            elif chars[i] == "+":
                if chars[i + 1] == "+":
                    chars[i + 1] = ""
                elif chars[i + 1] == "-":
                    chars[i] = "-"
                    chars[i + 1] = ""
            elif chars[i] == "-":
                if chars[i + 1] == "+":
                    chars[i + 1] = ""
                elif chars[i + 1] == "-":
                    chars[i] = "+"
                    chars[i + 1] = ""
            elif chars[i] == "[":
                chars[i] = "{"
            elif chars[i] == "]":
                chars[i] = "}"
            elif chars[i] == "/":
                chars[i] = "\over"

    output = ""
    for char in chars:
        output += str(char)

    return output


def internerLink(target):
    return target


def test():
    plschange = f"""

    """

    if plschange.rstrip() == "":
        plschange = input("Welche Formel soll umgewandelt werden?\n")

    print("\n\n\n\n")
    print(change(plschange))
