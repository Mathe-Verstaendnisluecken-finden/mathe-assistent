import os
from django.core.management.base import BaseCommand
from django.core.management import call_command
from django.conf import settings

# noinspection PyUnresolvedReferences
from mainapp import models
# noinspection PyUnresolvedReferences
from mainapp import core

from ipydex import IPS

page_id_to_db_obj_mapping = {}


class Command(BaseCommand):
    """
    Run the reasoner and load relevant objects into database

    """

    help = "Load md files into the database"

    def add_arguments(self, parser):
        # Positional arguments
        # parser.add_argument('poll_ids', nargs='+', type=int)

        # Named (optional) arguments

        parser.add_argument(
            "--no-reasoner",
            action="store_true",
            help="omit running the reasoner (increase performance during testing)",
        )

        parser.add_argument(
            "--interactive",
            action="store_true",
            help="start ipython shell after finishing command (useful during testing)",
        )

        parser.add_argument(
            "--silent",
            action="store_true",
            help="suppress any output (useful for automatic running)",
        )

    def handle(self, *args, **options):
        call_command("flush", verbosity=0, interactive=False)

        path = os.path.join(settings.BASEDIR, "mainapp", "content")
        list_of_dicts = core.get_all_files(path)

        for data_dict in list_of_dicts:
            convert_dict(data_dict)

        for page_id, doc_db_obj in page_id_to_db_obj_mapping.items():

            if isinstance(doc_db_obj, models.OverviewDocument):

                upper_level_str = doc_db_obj.tmp_upper_level

                if upper_level_str is not None:
                    upper_level_db_obj = page_id_to_db_obj_mapping[upper_level_str]
                    doc_db_obj.upper_level = upper_level_db_obj
                    doc_db_obj.save()

            elif isinstance(doc_db_obj, models.ContentDocument):

                category_obj = page_id_to_db_obj_mapping.get(doc_db_obj.tmp_category_str)
                doc_db_obj.category = category_obj
                doc_db_obj.save()

                for req_str in doc_db_obj.tmp_requirement_strs:
                    req_obj = page_id_to_db_obj_mapping.get(req_str)


                    if req_str != "" and not req_obj:
                        # if requirement_string is not recognized
                        # (and not empty (->no req.)): raise error
                        raise ValueError(f"unkown requirement key: {req_str}")

                    doc_db_obj.requirements.add(req_obj)


            else:
                msg = f"Unexpected type: {type(doc_db_obj)}"

        create_menu_objects()

        if options.get("interactive"):
            # this is usefule during development
            IPS(print_tb=False)

        if not options.get("silent"):
            self.stdout.write(self.style.SUCCESS("Done"))


def convert_dict(data_dict):
    """
    Konvertiert eine MD-Datei in ein DB-Objekt

    :param data_dict:
    :return:
    """

    ct = data_dict["content_type"][0]

    if ct == "overview":
        create_overview_doc(data_dict)
    elif ct == "article":
        create_content_doc(data_dict)
    else:
        msg = f"Unkown content_type: {ct}"
        raise TypeError(msg)


def create_overview_doc(data_dict):
    level = int(data_dict["level"][0])
    page_id = data_dict["page_id"][0]
    page_name = data_dict["page_name"][0]
    content = data_dict["md_content"]
    file_path = data_dict["file_path"]

    obj = models.OverviewDocument(level=level, page_id=page_id, page_name=page_name,
                                  content=content, file_path=file_path)
    obj.save()

    obj.tmp_upper_level = data_dict.get("upper_level", [None])[0]

    page_id_to_db_obj_mapping[page_id] = obj


def create_content_doc(data_dict):
    page_id = data_dict["page_id"][0]
    page_name = data_dict["page_name"][0]
    content = data_dict["md_content"]
    file_path = data_dict["file_path"]
    exercise_path = data_dict["exercise_path"][0]

    obj = models.ContentDocument(category=None, page_id=page_id, page_name=page_name,
                                 content=content, file_path=file_path, exercise_path=exercise_path)
    obj.save()

    obj.tmp_category_str = data_dict["sub_category"][0]
    obj.tmp_requirement_strs = data_dict.get("requirements", [])

    page_id_to_db_obj_mapping[page_id] = obj


def create_menu():
    menu_main_entry_list = []
    menu_sub_entry_list = []

    all_overview_docs = models.OverviewDocument.objects.all()

    for overview_doc in all_overview_docs:
        if overview_doc.level == 1:
            dict = {
                "page_name": overview_doc.page_name,
                "page_id": overview_doc.page_id
            }
            menu_sub_entry_list.append(dict)

    for overview_doc in all_overview_docs:
        if overview_doc.level == 0:
            sub_entry_list = []
            for overview_dict in menu_sub_entry_list:
                if overview_dict["page_id"] == overview_doc.page_id:
                    sub_entry_list.append(overview_dict)
            dict = {
                "page_name": overview_doc.page_name,
                "page_id": overview_doc.page_id,
                "sub_menus": sub_entry_list
            }
            menu_main_entry_list.append(dict)

    return menu_main_entry_list


def create_menu_objects():
    entry_id_to_db_obj_mapping = {}
    all_overview_docs = models.OverviewDocument.objects.all()

    for overview_doc in all_overview_docs:
        level = int(overview_doc.level)
        if level == 1:
            entry_name = overview_doc.page_name
            entry_id = overview_doc.page_id
            obj = models.MenuEntry(level=level, entry_name=entry_name, entry_id=entry_id)
            obj.save()

            obj.tmp_upper_entry = overview_doc.upper_level.page_id
            entry_id_to_db_obj_mapping[entry_id] = obj

        elif level == 0:
            entry_name = overview_doc.page_name
            entry_id = overview_doc.page_id
            obj = models.MenuEntry(level=level, entry_name=entry_name, entry_id=entry_id)
            obj.save()
        else:
            msg = "unknown level: " + {str(overview_doc.level)}
            raise TypeError(msg)

    for entry_id, doc_db_obj in entry_id_to_db_obj_mapping.items():
        upper_entry_id = doc_db_obj.tmp_upper_entry
        if upper_entry_id is not None:
            upper_entry_obj = models.MenuEntry.objects.filter(entry_id=upper_entry_id).first()
            upper_entry_obj.sub_categorys.add(doc_db_obj)
