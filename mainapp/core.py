import markdown
import glob
from ipydex import IPS


def get_all_files(path):
    list_of_files_names = glob.glob(path + '/*.md')

    list_of_dicts = []
    for file_path in list_of_files_names:
        md_src = read_file(file_path)
        list_of_dicts.append(md_src)

    return list_of_dicts


def create_menu(path):
    list_of_dicts = get_all_files(path)

    list_of_overview_dicts = []

    for dictionary in list_of_dicts:
        if dictionary["content_type"][0] == "overview":
            list_of_overview_dicts.append(dictionary)

    return list_of_overview_dicts


def read_header(path_to_file):
    md = markdown.Markdown(extensions=['meta'])
    with open(path_to_file, "r") as txtfile:
        md.convert(txtfile.read())

    md_meta = md.Meta

    return dict(**md_meta)


def read_file(path_to_file):
    md = markdown.Markdown(extensions=['meta'])
    with open(path_to_file, "r") as txtfile:
        md_content = md.convert(txtfile.read())

    md_meta = md.Meta

    return dict(**md_meta, md_content=md_content, file_path=path_to_file)
