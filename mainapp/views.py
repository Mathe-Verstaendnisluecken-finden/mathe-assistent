import os
import sys
import json
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseServerError, HttpResponseRedirect
from django.urls import reverse
from django.conf import settings
import markdown
import glob

import importlib.util

from django.shortcuts import get_object_or_404

from .core import create_menu, read_header
from . import models

from ipydex import IPS

def home_page_view(request):
    path = os.path.join(settings.BASEDIR, "mainapp", "content", "static", "landingpage.md")
    with open(path, "r") as txtfile:
        md_src = txtfile.read()

    context = add_menu(dict(md_src=md_src))
    return render(request, 'mainapp/landingpage.html', context)


def md_inhalt_view(request, content_code=None):

    # load markdown sources into variable
    markdown.Markdown(extensions=['meta'])
    """create the renderd text from markdown & / or script - sources"""
    exp = ""

    if content_code:
        # page = models.ContentDocument.objects.filter(page_id=content_code).first()

        page = get_object_or_404(models.ContentDocument, page_id=content_code)
        path = page.file_path

        requirement_objects = page.requirements.all()
        requirements = []
        for req in requirement_objects:
            name = req.page_name
            link = req.page_id
            requirements.append([name, link])


        with open(path, "r") as txtfile:
            md_src = txtfile.read()

        if page.exercise_path:
            current_dir = os.path.dirname(os.path.abspath(sys.modules.get(__name__).__file__))
            spec = importlib.util.spec_from_file_location(page.exercise_path,
                                                          f"{current_dir}/content/exercise_generators/{page.exercise_path}.py")
            exercise = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(exercise)

            exp = exercise.getAufgabe()

        else:
            exp = "Leider keine automatisch generierten Aufgaben"

    else:
        md_src = "Diese Seite hat keinen Inhalt mit Markdown-Ursprung"

    context = add_menu(dict(md_src=md_src, exp=exp, requirements=requirements, title=page.page_name))

    return render(request, 'mainapp/md_inhalt.html', context)


def overview_view(request, page_code=None):
    markdown.Markdown(extensions=['meta'])
    page = models.OverviewDocument.objects.filter(page_id=page_code).first()
    path = page.file_path
    with open(path, "r") as txtfile:
        description = txtfile.read()
    header = read_header(path)
    links = create_overview(page.page_id)

    context = add_menu(dict(linklist=links, title=page.page_name, md_src=description))
    return render(request, 'mainapp/overview.html', context)


def imprint_view(request, page_code=None):
    markdown.Markdown(extensions=['meta'])

    path = os.path.join(settings.BASEDIR, "mainapp", "content", "static", page_code + ".md")
    md_meta = read_header(path)
    with open(path, "r") as txtfile:
        md_src = txtfile.read()

    context = add_menu(dict(md_src=md_src, header=md_meta))
    return render(request, 'mainapp/imprint.html', context)


def create_overview(page_id):
    list_of_overviews = models.OverviewDocument.objects.all()
    list_of_links = [[]]

    for file in list_of_overviews:
        if file.level != 0:
            if file.upper_level.page_id == page_id:
                list_of_links[0].append([file.page_id, file.page_name])

    list_of_links.append([])

    list_of_articles = models.ContentDocument.objects.all()
    for file in list_of_articles:
        if file.category is None:
            continue

        if file.category.page_id == page_id:
            list_of_links[1].append([file.page_id, file.page_name])

    return list_of_links


def debug_view(request, xyz=0):

    z = 1

    if xyz == 1:
        # start interactive shell for debugging (helpful if called from the unittests)
        IPS()

    elif xyz == 2:
        return HttpResponseServerError("Errormessage")

    return HttpResponse('Some plain message')


def add_menu(context):
    main_menu_entrys = models.MenuEntry.objects.filter(level=0).all()
    return dict(**context, menu=main_menu_entrys)
