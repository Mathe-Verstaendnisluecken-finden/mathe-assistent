# General Information

This directory contains a script and auxilliary files to deploy the containing django project on uberspace webhosting service. Other services need adaptions.
The deployment technique is largely based on [moodpoll](moodpoll.uber.space/).


# Deployment Information
## General

We use [deploymentutils](https://codeberg.org/cknoll/deploymentutils) (which is built on top of  [fabric](https://www.fabfile.org/) (>=2.5). This decision seems to be a good compromise between raw bash scripts and a complex configuration management system like ansible – at least for python affine folks.

Complete deployment should (at best) be an onliner.

## Preparation:

- Ensure you have this directory structure:

```
    [project_root]
    ├── [project_repo]/
    │   ├── .git/...
    │   ├── manage.py    │
    │   ├── deployment/
    │   │   ├── README.md                  <- you read this file
    │   │   ├── deploy.py
    │   │   ├── general/...                <- general deployment files
    │   │   ├── uberspace/...              <- uberspace-specific deployment files
    │   │   ├── config_example.ini         <- copy (two levels up) and adapt this file
    │   │   └──  ...
    │   └── ...
    │
    ├── config.ini                         <- contains your configuration (including secrets => outside repo)
    │                                         (You have to create this file manually. Use the example as template.)
    └── ...
```

## Local deployment

- `cd <project_repo>`
- `python3 manage.py migrate`
- `python3 manage.py runserver`


## Remote deployment on ([uberspace](https://uberspace.de/)):


### Preparation

- Create an [uberspace](https://uberspace.de)-account (first month is free), then pay what you like.
- Set up your ssh key in the webfrontend

On your local machine:

- `cd <project_repo>`
- `pip install deployment_requirements.txt`
- Create your config file:
    - `cp deployment/config_example.ini ../config.ini`
    - adapt contents (server access data, secret key, wikiadminpw ...)

### Deployment

- `cd <project_repo>/deployment`
- Inspect `deploy.py` and (optionally) have a look what happens in the remainder of that script.
    - If you use uberspace, you probably only need to change the username
    - Note `deploy.py` is heavily based on <https://lab.uberspace.de/guide_django.html>.
- Run `eval $(ssh-agent); ssh-add -t 5m` to unlock you private ssh-key in this terminal (The deplyment script itself does not ask for your ssh-key password).
- Run `python3 deploy.py -h` to get an overview of available options
- Run `python3 deploy.py --initial remote`.
