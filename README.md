# Link zu einer Version zu Demonstrationszwecken

https://test3.uber.space/

# Über das Projekt

Wenn im (Mathematik-) Unterricht Verständnisschwierigkeiten auftreten, liegt dies oft nicht nur primär an den gerade gestellten Aufgaben, manchmal fehlt auch das stillschweigend vorausgesetzte Wissen aus vorher behandelten Themenbereichen.

Diese Webanwendung soll in diesem Fall dazu dienen, dass sich der Nutzer über den **Voraussetzungen** - Teil schnell einen Überblick über das benötigte Grundwissen und eventuell auch die dafür benötigten Inhalte verschaffen kann.

